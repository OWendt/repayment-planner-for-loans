# General

This project was created as part of a coding challenge.

It can be used to calculate a replayment plan for loans.

# How To Run

* Install libraries `cd ./backend && npm install && cd ../frontend && npm install`
* Start backend and frontend `cd ./backend && npm start` and `cd ./frontend && npm start`
* Visit "http://localhost:3001/"

# Technology

* NestJS as Backend
* React and MaterialUI for the frontend

# What can be improved

* i18n
* query params can be added to frontend
* formatting and linting
* caching
* move the calculation completely to the frontend to avoid the complete backend (the current seperation was a requirement in the coding challenge)
* avoid duplicated types.ts which have to keept in sync
