import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RequestData } from './types';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('calculation', () => {
    it('handles longer period than necessary', () => {
      const input: RequestData = {total: 100, interest: 2, redemption: 10, duration: 1}
      const {monthlyRate, balanceDue,schedule} = appController.getResult(input)
      expect(monthlyRate).toBe(1)
      expect(balanceDue).toBe(89.91)
      expect(schedule.length).toBe(1)
    })
    it('handles longer period than necessary', () => {
      const input: RequestData = {total: 500, interest: 2, redemption: 10, duration: 10}
      const {monthlyRate, balanceDue,schedule} = appController.getResult(input)
      expect(monthlyRate).toBe(5)
      expect(balanceDue).toBe(0)
      expect(schedule.length).toBe(10)
    })
  })
});
