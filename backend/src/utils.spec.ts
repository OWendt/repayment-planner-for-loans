import { getMonthlyRate, getMonthlyRedemptionPlan, aggregateRedemptionPlans, getSchedule } from './utils';

describe('utils', () => {
    it('getMonthlyRate', () => { 
        expect(getMonthlyRate({total: 100, interest: 2, redemption: 5})).toBe(7/12)
    })
    it('getMonthlyRedemptionPlan', () => {
        const input = {
            rest: 100,
            interest: 2,
            monthlyRate: 6
        }
        const expectedOutcome = {
            rest: 94.17, interest: 0.17, redemption: 5.83, rate: 6
        }
        expect(getMonthlyRedemptionPlan(input)).toStrictEqual(expectedOutcome)
    })
    it ('aggregateRedemptionPlans', () => {
        expect(aggregateRedemptionPlans([
            {interest: 2, redemption: 3, rest: 97, rate: 5},
            {interest: 1, redemption: 4, rest: 93, rate: 5},
        ])).toStrictEqual({interest: 3, redemption: 7, rest: 93, rate: 10})
    })
    it('getSchedule one year', () => {
        expect(getSchedule({interest: 2, monthlyRate: 5, total: 500, duration: 1})).toStrictEqual([{year: 1, interest: 9.54, redemption: 50.46, rest: 449.54, rate: 60}])
    })
    it('getSchedule two years', () => {
        expect(getSchedule({interest: 2, monthlyRate: 5, total: 500, duration: 2})[1]).toStrictEqual({year: 2, interest: 8.53, redemption: 51.47, rest: 398.07, rate: 60})
    })
    it('getSchedule when duration is longer than needed', () => {
        expect(getSchedule({interest: 2, monthlyRate: 100, total: 100, duration: 1})).toStrictEqual([{year: 1, interest: 0.17, redemption: 100, rest: 0, rate: 100.17}])
    })
})
