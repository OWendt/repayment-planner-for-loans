import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { RequestData, ResponseData } from './types';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getResult(@Query() query: RequestData): ResponseData { 
    return this.appService.getResult(query)
  }
}
