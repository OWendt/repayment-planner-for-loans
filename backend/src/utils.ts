import { RedemptionPlan, RedemptionPlanWithYear, RequestData } from "./types"

export const getMonthlyRate = ({ interest, redemption, total }: Omit<RequestData, 'duration'>): number => {
    const redemptionPortion = total * (redemption / 100)
    const interestPortion = total * (interest / 100)
    return (redemptionPortion + interestPortion) / 12
}

interface GetMonthlyRedemptionPlanProps {
    interest: number,
    monthlyRate: number,
    rest: number
}

export const getMonthlyRedemptionPlan = ({ interest, monthlyRate, rest }: GetMonthlyRedemptionPlanProps): RedemptionPlan => {
    const interestPortion = Number((rest * (interest / 100) / 12).toFixed(2))
    if (rest < monthlyRate) {
        return { rest: 0, interest: interestPortion, redemption: rest - interestPortion, rate: rest }
    }
    const redemptionPortion = Number((monthlyRate - interestPortion).toFixed(2))
    const newRest = Number((rest - redemptionPortion).toFixed(2))
    return {
        rest: newRest, interest: interestPortion, redemption: redemptionPortion, rate: redemptionPortion + interestPortion
    }
}

export const getYearlyRedemptionPlan = (props: GetMonthlyRedemptionPlanProps): RedemptionPlan => {
    const schedules: RedemptionPlan[] = [getMonthlyRedemptionPlan(props)]
    for (let month = 1; month < 12; month++) {
        schedules.push(getMonthlyRedemptionPlan({ ...props, rest: schedules[schedules.length - 1].rest }))
    }
    return aggregateRedemptionPlans(schedules)
}

export const aggregateRedemptionPlans = (monthlyPlans: RedemptionPlan[]): RedemptionPlan =>
    monthlyPlans.reduce((aggregator, nextValue) => ({
        interest: Number((aggregator.interest + nextValue.interest).toFixed(2)),
        redemption: Number((aggregator.redemption + nextValue.redemption).toFixed(2)),
        rest: Number((nextValue.rest).toFixed(2)),
        rate: Number((aggregator.rate + nextValue.rate).toFixed(2)),
    })
        , { interest: 0, redemption: 0, rest: 0, rate: 0 })

export const getSchedule = ({ interest, monthlyRate, total, duration }: Omit<RequestData, 'redemption'> & {monthlyRate: number}): RedemptionPlanWithYear[] => {
    const schedule: RedemptionPlanWithYear[] = [{ ...getYearlyRedemptionPlan({ interest: interest, monthlyRate, rest: total }), year: 1 }]
    while (schedule.length < duration) {
        const { rest: lastRest, year: lastYear } = schedule[schedule.length - 1]
        schedule.push({ ...getYearlyRedemptionPlan({ interest: interest, monthlyRate, rest: lastRest }), year: lastYear + 1 })
    }
    return schedule
}