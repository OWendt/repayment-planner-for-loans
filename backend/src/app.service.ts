import { Injectable } from '@nestjs/common';
import { RequestData, ResponseData } from './types';
import { getMonthlyRate, getSchedule } from './utils';

@Injectable()
export class AppService {
  getResult(props: RequestData): ResponseData {
    if (!props.duration || !props.redemption || !props.total || !props.interest) {
      return undefined
    }
    const monthlyRate = getMonthlyRate(props)
    const schedule = getSchedule({...props, monthlyRate})
    return { monthlyRate, balanceDue: schedule[schedule.length - 1].rest, schedule };
  }
}

