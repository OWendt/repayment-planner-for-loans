export interface RequestData {
    total: number,
    interest: number,
    redemption: number,
    duration: number
}

export type RedemptionPlanWithYear = RedemptionPlan & {
    year: number
}

export interface RedemptionPlan {
    rate: number,
    redemption: number,
    interest: number,
    rest: number
}

export interface ResponseData {
    monthlyRate: number,
    balanceDue: number,
    schedule: RedemptionPlanWithYear[]
}

