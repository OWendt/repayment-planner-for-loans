import { InputAdornment } from "@mui/material"
import { MyPaper, MyTextField } from "./StyledComponents"
import { RequestData } from './types';

interface ParameterInputProps {
    onChange: <K extends keyof RequestData>(key: K, limits?: { min: number, max: number }) => (e: any) => void;
    params: Partial<RequestData>;
}

export const ParameterInput = ({ onChange, params }: ParameterInputProps) => {
    const percentAdornment = {
        InputProps: {
            endAdornment: <InputAdornment position="end">%</InputAdornment>,
        }
    }

    const euroAdornment = {
        InputProps: {
            endAdornment: <InputAdornment position="end">€</InputAdornment>,
        }
    }

    return (<MyPaper sx={{ display: 'flex', margin: 'auto', justifyContent: 'space-around' }}>
        <MyTextField
            type='number'
            label="Darlehensbetrag"
            value={params.total ?? ''}
            onChange={onChange('total')}
            {...euroAdornment} />
        <MyTextField
            type='number'
            value={params.interest ?? ''}
            onChange={onChange('interest')}
            label="Sollzinssatz"
            {...percentAdornment} />
        <MyTextField
            type='number'
            label="Anfängliche Tilgung"
            value={params.redemption ?? ''}
            onChange={onChange('redemption')}
            {...percentAdornment} />
        <MyTextField
            type='number'
            label="Zinsbindungsdauer"
            value={params.duration ?? ''}
            onChange={onChange('duration', { min: 1, max: 30 })}
            InputProps={{
                endAdornment: <InputAdornment position="end">J</InputAdornment>,
            }} />
    </MyPaper>)
}