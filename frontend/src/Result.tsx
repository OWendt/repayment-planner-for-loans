import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material"
import { ResponseData } from './types'
import { MyPaper } from "./StyledComponents"

export const Result = ({ data }: { data: ResponseData }) => (
    <>
        <MyPaper sx={{ display: 'flex', flexDirection: 'column', padding: 2 }}>
            <Box sx={{ margin: 'auto' }}>
                <Typography>Monatliche Rate: {data.monthlyRate.toFixed(2)} €</Typography>
                <Typography>Restschuld: {data.balanceDue} € </Typography>
            </Box>
        </MyPaper>

        <TableContainer component={Paper}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Jahr</TableCell>
                        <TableCell>Rate</TableCell>
                        <TableCell>Zinsanteil</TableCell>
                        <TableCell>Tilgungsanteil</TableCell>
                        <TableCell>RestSchuld</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.schedule.map(({ year, rate, interest, redemption, rest }) => <TableRow key={year}>
                        <TableCell>{year}</TableCell>
                        <TableCell>{rate}</TableCell>
                        <TableCell>{interest}</TableCell>
                        <TableCell>{redemption}</TableCell>
                        <TableCell>{rest}</TableCell>
                    </TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>
    </>


)