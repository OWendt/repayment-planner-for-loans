import { Box, Grid, Typography } from "@mui/material"
import React, { ReactElement, useEffect, useState } from "react"
import { useDebounce } from "usehooks-ts";
import { RequestData, ResponseData } from './types';
import { ParameterInput } from "./ParameterInput";
import { Result } from "./Result";

const defaultParams: RequestData = { interest: 2, redemption: 10, total: 500, duration: 5 }

export const App = (): ReactElement => {
  const [params, setParams] = useState<Partial<RequestData>>(() => defaultParams)
  const debouncedParams = useDebounce<Partial<RequestData>>(params, 200)
  const [data, setData] = useState<ResponseData | undefined>()

  useEffect(() => {
    const fetchData = async () => {
      const { total, redemption, interest, duration } = debouncedParams
      const response = await fetch(`http://localhost:3000/?total=${total}&interest=${interest}&redemption=${redemption}&duration=${duration}`)
      const responseData: ResponseData = await response.json()
      if (!responseData.monthlyRate) {
        setData(undefined)
        return
      }
      setData(responseData)
    }
    fetchData()
  }, [debouncedParams])

  const onChange = <K extends keyof RequestData>(key: K, limits?: { min: number, max: number }) => (e: any) => {
    const newValue = parseFloat(e.target.value)
    if (newValue < 0) {
      return
    }
    if (limits && (newValue < limits.min || newValue > limits.max)) {
      return
    }
    setParams((cur) => ({ ...cur, [key]: isNaN(newValue) ? null : newValue }))
  }

  return (
    <>
      <Box sx={{ display: 'flex', justifyContent: 'space-around' }}>
        <Typography variant="h4" sx={{ marginBottom: 2 }}>Tilgungsplan für Kredite</Typography>
      </Box>
      <Grid container justifyContent='center'>
        <Grid item xs={12} sm={9} xl={7}>
          <ParameterInput params={params} onChange={onChange} />
        </Grid>
        {data !== undefined && <Grid item xs={12} sm={9} xl={7}>
          <Result data={data} />
        </Grid>
        }
      </ Grid>
    </>
  )
}

