import styled from "@emotion/styled";
import { Paper, TextField } from "@mui/material";

export const MyPaper = styled(Paper)({
    margin: 8
})

export const MyTextField = styled(TextField)({
    margin: 8
})